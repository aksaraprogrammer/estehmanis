<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    protected $datalist = [];
    protected $datalistEmployees = [];
    protected $datalistAdmins = [];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $faker = Faker::create("id_ID");

        DB::beginTransaction();

        // admins
        DB::table("admins")->truncate();
        for ($i = 0; $i < 100; $i++) {
            $account = $faker->shuffleString("abcdeABCDE", "UTF-8");
            try {
                $this->datalistAdmins[] = [
                    'user_id' => $account,
                    'user_pass' => $account,
                    'email_account' => $account . '@estehmanispos.localdomain',
                    'wa_number' => $faker->numerify("+62851-####-####"),
                    'city' => $faker->city(),
                    'postcode' => $faker->postcode(),
                ];
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        DB::table("admins")->insert($this->datalistAdmins);

        DB::table("employees")->truncate();
        for ($i = 0; $i < 100; $i++) {
            $account = $faker->shuffleString("abcdeABCDE", "UTF-8");
            try {
                $this->datalistEmployees[] = [
                    'ktp' => $faker->numerify("2024-###-###-###"),
                    'nama' => $faker->name,
                    'alamat' => $faker->address,
                    'tempat_lahir' => $faker->city(),
                    'tanggal_lahir' => $faker->date,
                    'st_menikah' => null,
                    'st_karyawan' => null,
                    'aktif' => FALSE,
                    'wa' => $faker->numerify("+62851-####-####"),
                ];
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        DB::table("employees")->insert($this->datalistEmployees);

        DB::table("bank_account")->truncate();
        DB::table("bank_account")->insert([
            [
                'kode' => 'BNI',
                'keterangan' => 'PT Bank Negara Indonesia'
            ],
            [
                'kode' => 'BCA',
                'keterangan' => 'PT Bank Central Asia'
            ],
            [
                'kode' => 'BRI',
                'keterangan' => 'PT Bank Rakyat Indonesia'
            ],
        ]);

        DB::table("payment_method")->truncate();
        DB::table("payment_method")->insert([
            [
                'kode' => 'CASH',
                'keterangan' => 'Bayar Tunai'
            ],
            [
                'kode' => 'TF',
                'keterangan' => 'Bayar Transfer'
            ],
            [
                'kode' => 'QRIS',
                'keterangan' => 'Bayar QRIS'
            ],
            [
                'kode' => 'OVO',
                'keterangan' => 'Bayar OVO'
            ],
        ]);

        DB::commit();
    }
}
