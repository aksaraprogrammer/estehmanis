<?php

use App\Http\Controllers\BankAccountController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (!session()->has("user_id")) {
        return redirect()->to("login");
    }
    return view('home');
});

Route::any('login', [LoginController::class, 'index']);
Route::any('register', [RegisterController::class, 'index']);
Route::get('logout', function () {
    session()->flush();
    return redirect()->to("login");
});

Route::resource("bank-account", BankAccountController::class);
Route::resource("payment-method", PaymentMethodController::class);
Route::resource("employee", EmployeeController::class);
Route::post("employee/datatable", [EmployeeController::class, 'datatable']);
Route::post("employee/aktif", [EmployeeController::class, 'aktif']);