<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $table = "employees";

    protected $fillable = [
        "ktp",
        "nama",
        "alamat",
        "wa",
        "tempat_lahir",
        "tanggal_lahir",
        "st_menikah",
        "st_karyawan"
    ];

    public function getStatusKaryawan()
    {
        return [
            0 => 'Belum Bekerja',
            'Tetap',
            'Kontrak',
            'Freelance',
            'Seasonal'
        ];
    }

    public function getStatusMenikah()
    {
        return [
            0 => 'Belum Menikah',
            'Menikah',
            'Janda',
            'Duda'
        ];
    }

    public function insertData($data)
    {
        try {
            return Employees::firstOrCreate($data);
        } catch (\Throwable $th) {
            $content = [
                "code" => $th->getCode(),
                "message" => $th->getMessage()
            ];
            \Storage::append("error.log", json_encode($content));
        }
    }

    public function updateData($data, $id)
    {
        try {
            return Employees::where("id",$id)->update($data);
        } catch (\Throwable $th) {
            $content = [
                "code" => $th->getCode(),
                "message" => $th->getMessage()
            ];
            \Storage::append("error.log", json_encode($content));
        }
    }

    public function getDataById($id)
    {
        return Employees::where("id", $id)->firstOrFail();
    }
}
