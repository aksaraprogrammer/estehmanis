<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admins extends Model
{
    use HasFactory;

    protected $table = "admins";

    public function loginAuth($user_id, $user_pass)
    {
        return DB::table("admins")
            ->where("user_id", $user_id)
            ->where("user_pass", $user_pass)
            ->first();
    }

    public function registerAuth(
        $user_id,
        $user_pass,
        $email_account,
        $wa_number
    ) {
        try {
            return DB::table("admins")->insert([
                'user_id' => $user_id,
                'user_pass' => $user_pass,
                'email_account' => $email_account,
                'wa_number' => $wa_number,
            ]);
        } catch (\Throwable $th) {
            $content = [
                "code" => $th->getCode(),
                "message" => $th->getMessage()
            ];
            \Storage::append("error.log", json_encode($content));
        }
    }
}
