<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    public function index(Request $request)
    {
        $param['banks'] = PaymentMethod::paginate(4);
        return view("paymentmethod.index", $param);
    }

    public function store(Request $request)
    {
        PaymentMethod::create($request->all());
        return redirect()->back()->with("error", "Payment Method Stored");
    }

    public function show($id)
    {
        return PaymentMethod::where("id", $id)->firstOrFail();
    }

    public function update(Request $request, $id)
    {
        PaymentMethod::where("id", $id)->update($request->except(['_token', '_method']));
        return redirect()->back();
    }

    public function destroy($id)
    {
        return PaymentMethod::where("id", $id)->delete();
    }
}
