<?php

namespace App\Http\Controllers;

use App\Models\Admins;
use DB;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod("POST")) {
            $model = new Admins();
            $user = $model->loginAuth($request->user_id, $request->user_pass);
            if (!empty($user)) {
                session()->start();
                session()->put("user_id", $user->user_id);
                return redirect()->to("/");
            } else {
                return redirect()->back();
            }
        }
        return view("common.login");
    }
}
