<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    public function index(Request $request)
    {
        $param['banks'] = BankAccount::paginate(4);
        return view("bankaccount.index", $param);
    }

    public function store(Request $request)
    {
        BankAccount::create($request->all());
        return redirect()->back()->with("error", "Bank Account Stored");
    }

    public function show($id)
    {
        return BankAccount::where("id", $id)->firstOrFail();
    }

    public function update(Request $request, $id)
    {
        BankAccount::where("id", $id)->update($request->except(['_token', '_method']));
        return redirect()->back();
    }

    public function destroy($id)
    {
        return BankAccount::where("id",$id)->delete();
    }
}
