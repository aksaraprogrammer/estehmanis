<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmployeeRequest;
use App\Models\Employees;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        return view("employee.index");
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $model = Employees::query();
            return DataTables::eloquent($model)
                ->addColumn("aksi", "employee.aksi")
                ->addColumn("aktif", "employee.aktif")
                ->rawColumns(['aksi', 'aktif'])
                ->make(true);
        }
    }

    public function create(Request $request)
    {
        $model = new Employees();
        $param['model'] = $model;
        return view("employee.create", $param);
    }

    public function store(
        Request $request,
        StoreEmployeeRequest $storeEmployeeRequest
    ) {
        $input = $storeEmployeeRequest->except(['_token', '_method']);
        $validated = $storeEmployeeRequest->validated();
        dd($validated);
        if ($validated) {
            $model = Employees::insertData($input);
            return redirect()->to("/employee/" . $model->id);
        }
    }

    public function update(
        Request $request,
        StoreEmployeeRequest $storeEmployeeRequest,
        $id
    ) {
        $input = $storeEmployeeRequest->except(['_token', '_method']);
        $validated = $storeEmployeeRequest->validated();
        if ($validated) {
            Employees::updateData($input, $id);
            return redirect()->to("/employee/" . $id);
        }
    }

    public function show($id)
    {
        $params['model'] = Employees::getDataById($id);
        return view("employee.show", $params);
    }

    public function edit(Request $request, $id)
    {
        $params['model'] = Employees::getDataById($id);
        return view("employee.edit", $params);
    }

    public function destroy(Request $request, $id)
    {
        Employees::where("id", $id)->delete();
        return redirect()->to("/employee");
    }

    public function aktif(Request $request)
    {
        $aktif = ($request->aktif !== "true") ? "0" : "1";
        Employees::where("id", $request->id)->update(['aktif' => $aktif]);
        return response()->json($request->all());
    }
}
