<?php

namespace App\Http\Controllers;

use App\Models\Admins;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod("POST")) {
            $model = new Admins();
            $user = $model->registerAuth(
                $request->user_id,
                $request->user_pass,
                $request->email_account,
                $request->wa_number
            );
            if (!empty($user)) {
                session()->start();
                session()->put("user_id", $request->user_id);
                return redirect()->to("/");
            } else {
                return redirect()->back();
            }
        }
        return view("common.register");
    }
}
