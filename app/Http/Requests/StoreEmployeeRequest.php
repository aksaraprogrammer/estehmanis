<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ktp' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'wa' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            "st_menikah" => "required",
            "st_karyawan" => "required"
        ];
    }

    public function except($keys)
    {
        return parent::except($keys);
    }

    public function messages()
    {
        return [
            'ktp.required' => 'ktp dibutuhkan',
            'nama.required' => 'nama dibutuhkan',
            'alamat.required' => 'alamat dibutuhkan',
            'wa.required' => 'wa dibutuhkan',
            // 'tempat_lahir.required' => 'tempat_lahir dibutuhkan',
            // 'tanggal_lahir.required' => 'tanggal_lahir dibutuhkan'
        ];
    }
}
