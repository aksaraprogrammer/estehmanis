<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <a class="navbar-brand" href="#">Dasbor</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/"> <i class="bi bi-inboxes-fill"></i> Home <span
                        class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-inboxes-fill"></i>
                    Data Master
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#"> Barang</a>
                    <a class="dropdown-item" href="/employee"> Karyawan</a>
                    <a class="dropdown-item" href="/bank-account"> Rekening Bank</a>
                    <a class="dropdown-item" href="/payment-method"> Metode Bayar</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-inboxes-fill"></i>
                    Pembelian
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Pesan Pembelian</a>
                    <a class="dropdown-item" href="#">Faktur Pembelian</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-inboxes-fill"></i>
                    Penjualan
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Pesan Penjualan</a>
                    <a class="dropdown-item" href="#">Faktur Penjualan</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-inboxes-fill"></i>
                    Laporan
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Penjualan</a>
                    <a class="dropdown-item" href="#">Pembelian</a>
                </div>
            </li>

            @if (session()->has("user_id"))
                <li class="nav-item ">
                    <a class="nav-link" href="/logout"> <i class="bi bi-inboxes-fill"></i> Logout <span
                            class="sr-only">(current)</span></a>
                </li>
            @endif
        </ul>
    </div>
</nav>