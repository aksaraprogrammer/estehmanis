@extends("welcome")

@section("content")
<h4 class="mt-4">Show Data</h4>
<section class="mt-4">

    <div class="row">
        <div class="form-group col-6">
            <label for="">KTP</label>
            <input readonly disabled type="text" class="form-control" name="ktp" value="{!! $model->ktp !!}">
        </div>
        <div class="form-group col-6">
            <label for="">NAMA</label>
            <input readonly disabled type="text" class="form-control" name="nama" value="{!! $model->nama !!}">
        </div>
        <div class="form-group col-6">
            <label for="">ALAMAT</label>
            <input readonly disabled type="text" class="form-control" name="alamat" value="{!! $model->alamat !!}">
        </div>
        <div class="form-group col-6">
            <label for="">TEMPAT LAHIR</label>
            <input readonly disabled type="text" class="form-control" name="tempat_lahir" value="{!! $model->tempat_lahir !!}">
        </div>
        <div class="form-group col-6">
            <label for="">TANGGAL LAHIR</label>
            <input readonly disabled type="text" class="form-control" name="tanggal_lahir" value="{!! $model->tanggal_lahir !!}">
        </div>
        <div class="form-group col-6">
            <label for="">WA</label>
            <input readonly disabled type="text" class="form-control" name="wa" value="{!! $model->wa !!}">
        </div>
        <div class="form-group col-6">
            <label for="">MENIKAH</label>
            <input readonly disabled type="text" class="form-control" name="wa" value="{!! $model->st_menikah !!}">
        </div>
        <div class="form-group col-6">
            <label for="">KARYAWAN</label>
            <input readonly disabled type="text" class="form-control" name="wa" value="{!! @old("st_karyawan", $model->st_karyawan) !!}">
        </div>
    </div>

    <div class="btn-group">
        <a href="/employee/{!! $model->id !!}/edit" type="submit" class="btn btn-warning">UBAH</a>
        <form action="/employee/{{ $model->id }}" method="POST" onclick="deleteData(event)">
            @csrf
            {!! @method_field("DELETE") !!}
            <button class="btn btn-danger" href="#">HAPUS</button>
        </form>
    </div>
</section>

<script>
    function deleteData(e)
    {
        e.preventDefault();
        if(confirm("lanjutkan menghapus data?")){
            $('form').submit();
        }
    }
</script>
@endsection