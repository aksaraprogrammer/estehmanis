@extends("welcome")

@section("content")
<h4 class="mt-4">Entri Data</h4>
<section class="mt-4">
    <form action="/employee" method="POST" enctype="multipart/form-data">
        @csrf
        {!! @method_field("POST") !!}

        @include("employee.entri")

        <button type="submit" class="btn btn-success">SIMPAN</button>
    </form>
</section>
@endsection