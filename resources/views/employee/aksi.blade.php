<div class="dropdown">
  <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Aksi
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="{!! url("/employee/".$model->id) !!}">Lihat</a>
    <form action="/employee/{{ $model->id }}" method="POST" onclick="deleteData(event)">
        @csrf
        {!! @method_field("DELETE") !!}
        <button class="dropdown-item" href="#">Hapus</button>
    </form>
  </div>
</div>

<script>
    function deleteData(e)
    {
        e.preventDefault();
        if(confirm("lanjutkan menghapus data?")){
            $('form').submit();
        }
    }
</script>