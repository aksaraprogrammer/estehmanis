@if ($errors->any())
    <div class="">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="form-group col-6">
        <label for="">KTP</label>
        <input type="text" class="form-control" name="ktp" value="{!! @old("ktp", $model->ktp) !!}">
    </div>
    <div class="form-group col-6">
        <label for="">NAMA</label>
        <input type="text" class="form-control" name="nama" value="{!! @old("nama", $model->nama) !!}">
    </div>
    <div class="form-group col-6">
        <label for="">ALAMAT</label>
        <input type="text" class="form-control" name="alamat" value="{!! @old("alamat", $model->alamat) !!}">
    </div>
    <div class="form-group col-6">
        <label for="">TEMPAT LAHIR</label>
        <input type="text" class="form-control" name="tempat_lahir" value="{!! @old("tempat_lahir", $model->tempat_lahir) !!}">
    </div>
    <div class="form-group col-6">
        <label for="">TANGGAL LAHIR</label>
        <input type="text" class="form-control" name="tanggal_lahir" value="{!! @old("tanggal_lahir", $model->tanggal_lahir) !!}">
    </div>
    <div class="form-group col-6">
        <label for="">WA</label>
        <input type="text" class="form-control" name="wa" value="{!! @old("wa", $model->wa) !!}">
    </div>
    <div class="form-group col-6">
        <label for="">St. MENIKAH</label>
        <select name="st_menikah" id="">
            @foreach ($model->getStatusMenikah() as $item)
            <option 
                {{ ($item == $model->st_menikah) ? "selected=true" : null }}
                value="{!! $item !!}" >{!! $item !!}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-6">
        <label for="">St. Karyawan</label>
        <select name="st_karyawan" id="">
            @foreach ($model->getStatusKaryawan() as $item)
            <option 
                {{ ($item == $model->st_karyawan) ? "selected=true" : null }}
                value="{!! $item !!}" >{!! $item !!}</option>
            @endforeach
        </select>
    </div>
</div>