@extends("welcome")

@section("content")
<section class="mt-4 mb-4">
    <h4>Employee</h4>

    <a href="/employee/create" class="btn btn-sm btn-primary mt-2">Data Baru</a>

    <div class="card mt-3" style="padding: 20px;">
        <table class="table" id="employeeList" style="width: 100%;">
            <thead>
                <th>ID</th>
                <th>KTP</th>
                <th>NAMA</th>
                <th>AKTIF</th>
                <th>ALAMAT</th>
                <th>WA</th>
                <th>AKSI</th>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</section>
@endsection

@push("styles")
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/3.0.2/css/responsive.bootstrap4.css">
@endpush

@push("scripts")
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/3.0.2/js/dataTables.responsive.js"></script>
    <script src="https://cdn.datatables.net/responsive/3.0.2/js/responsive.bootstrap4.js"></script>
    <script>
        new DataTable('#employeeList', {
            processing: true,
            serverSide: true,
            stateSave: true,
            responsive: true,
            pageLength: 5,
            // fixedHeader: true,
            // autoWidth: true,
            // pagingType: 'simple',
            ajax: {
                type: "post",
                url: "/employee/datatable"
            },
            layout: {
                bottomStart: {
                    pageLength: {
                        menu: [5, 10],
                    }
                },
                topStart: {
                    info: {
                        text: 'Data _START_ to _END_ of _TOTAL_'
                    }
                },
                bottomEnd: {
                    paging: {
                        numbers: 3
                    }
                },
                topEnd: {
                    search: {
                        placeholder: 'Mau cari apa?'
                    }
                },
            },
            columns: [
                { data: 'id' },
                { data: 'ktp' },
                { data: 'nama' },
                { data: 'aktif' },
                { data: 'alamat' },
                { data: 'wa' },
                { data: 'aksi' },
            ],
            ordering: false,
        });
    </script>

    <script>
        "use strict";

        function updateAktif(id) {
            const isChecked = $("#check" + id).is(":checked");
            $.ajax({
                type: "post",
                url: "/employee/aktif",
                data: {
                    id: id,
                    aktif: isChecked
                },
                success: function (response, status, xhr) {
                    console.log(response)
                },
                error: function (xhr, status) {
                    alert(status)
                }
            })
        }
    </script>

@endpush