<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Authentication</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>

<body>

    <style>

        .row {
            display: flex;
            flex-wrap: wrap;
            margin: auto;
            padding: auto;
        }

        .column {
            flex: 1;
            /* padding: 20px; */
            /* border: 1px solid #ddd; */
        }

        /* Responsive layout: Stack columns on small screens */
        @media screen and (max-width: 600px) {
            .column {
                flex-basis: 100%;
            }
        }

        .loginpage {
            width: 80%;
            margin: auto;
        }

        .loginpage h4 {
            font-size: 24pt;
        }

        .loginpage button {
            color: aliceblue;
        }

        .bannerbg {
            background-image: url('img/bg1.jpg');
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        
        .sectionfull {
            min-height: 100vh;
        }

    </style>

    <div class="row sectionfull">
        <div class="column bannerbg">
            <!-- Left column content -->
            <!-- Add your login form elements here -->
        </div>
        <div class="column" style="margin: auto;">
            <div class="loginpage">
                <h4>Login</h4>
                <form action="" method="post">
                    @csrf
                    {!! method_field("POST") !!}
                    <div class="form-group">
                        <label for="">User ID</label>
                        <input type="text" class="form-control" name="user_id" id="">
                    </div>
                    <div class="form-group">
                        <label for="">User Pass</label>
                        <input type="password" class="form-control" name="user_pass" id="">
                    </div>
                    <button type="submit" class="btn bg-success btn-block" style="line-height: 30px;">LOGIN</button>
                </form>
                

                <div class="text-center" style="margin-top: 40px;">
                <a href="/register">
                    <span>Register Account</span>
                </a>
                </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
        crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    -->

    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

</body>

</html>