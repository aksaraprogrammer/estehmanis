@extends("welcome")

@section("content")
<section class="mt-4">
    <h4>Bank Account</h4>

    <div class="row">
        <div class="col-md-6 col-12">
            @if (session()->has("error"))
                <div class="alert alert-info">
                    {!! session()->get("error") !!}
                </div>
            @endif
            <div class="card" style="padding: 20px;">
                <form method="post" action="{!! url("bank-account") !!}">
                    @csrf
                    {!! @method_field("POST") !!}
                    <div class="form-group">
                        <label for="">KODE</label>
                        <input type="text" name="kode" class="form-control" id="" required>
                    </div>
                    <div class="form-group">
                        <label for="">KETERANGAN</label>
                        <input type="text" name="keterangan" class="form-control" id="" required>
                    </div>
                    <button type="submit" class="btn btn-success">SIMPAN</button>
                </form>
            </div>
        </div>
        <div class="col-md-6 col-12">
            <div class="card table-responsive" style="width: 100%;">
                <table class="table table-striped" style="margin: 0; padding: 0;">
                    <thead>
                        <th>ID</th>
                        <th>KODE</th>
                        <th>KETERANGAN</th>
                        <th>#</th>
                    </thead>
                    <tbody>
                        @foreach ($banks as $bank)
                            <tr>
                                <td>{!! $bank->id !!}</td>
                                <td>{!! $bank->kode !!}</td>
                                <td>{!! $bank->keterangan !!}</td>
                                <td>
                                    <button class="btn btn-sm btn-primary btn-lihat"
                                        data-url="/bank-account/{{$bank->id}}">LIHAT</button>
                                    <button class="btn btn-sm btn-primary btn-hapus"
                                        data-url="/bank-account/{{$bank->id}}">HAPUS</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            {!! $banks->links("vendor.pagination.bootstrap-4") !!}
        </div>
    </div>
</section>
@endsection

@push("scripts")
    <script>
        $('.btn-lihat').on('click', function (event, handler) {
            var url = $(this).attr('data-url')
            $.ajax({
                type: "GET",
                url: url,
                success: function (data, status, xhr) {
                    $("input[name=kode]").val(data.kode)
                    $("input[name=keterangan]").val(data.keterangan)
                    $("input[name=_method]").val("PUT")
                    $("form").attr("action", url)
                },
                error: function (xhr, status, error) {
                    console.log(status)
                }

            })
        });

        $('.btn-hapus').on('click', function (event, handler) {
            var url = $(this).attr('data-url')
            if (confirm("lanjutkan menghapus data?")) {
                $.ajax({
                    type: "DELETE",
                    url: url,
                    success: function (data, status, xhr) {
                        window.location.reload()
                    },
                    error: function (xhr, status, error) {
                        console.log(status)
                    }

                })
            }
        });
    </script>
@endpush