@extends("welcome")

@section("content")
<section class="mb-4">
    <h4 class="mt-4">Hi! {!! session()->get("user_id") !!}</h4>

    <div class="row mt-4">
        <div class="col-12 col-sm-4">
            <a href="aktivasi/paket1">
                <div class="card subscribe">
                    <h3>Paket Basic</h3>
                    <h5>Rp. 55/bulan </h5>
                </div>
            </a>
        </div>

        <div class="col-12 col-sm-4">
            <a href="aktivasi/paket2">
                <div class="card subscribe">
                    <h3>Paket Premium</h3>
                    <h5>Rp. 70K/bulan </h5>
                </div>
            </a>
        </div>

        <div class="col-12 col-sm-4">
            <a href="aktivasi/paket3">
                <div class="card subscribe">
                    <h3>Paket Gold</h3>
                    <h5>Rp. 99K/bulan </h5>
                </div>
            </a>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-12 col-sm-6">
            <!-- HTML -->
            <div class="card" style="padding: 15px;">
                <h4 class="mt-2">Penjualan hari ini</h4>
                <div class="chartpanel" id="chartdiv1"></div>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <!-- HTML -->
            <div class="card" style="padding: 15px;">
                <h4 class="mt-2">Penjualan bulan ini</h4>
                <div class="chartpanel" id="chartdiv2"></div>
            </div>
        </div>
    </div>
</section>
@endsection

@push("styles")
    <style>
        a {
            text-decoration: none !important;
            color: black;
        }
        
        .chartpanel {
            height: 400px;
        }

        .card h4 {
            text-align: center;
        }

        #chartdiv2 {
            width: 100%;
            /* height: 500px; */
        }

        #chartdiv1 {
            width: 100%;
            /* height: 500px; */
        }

        .subscribe {
            padding-top: 30px;
            padding-bottom: 30px;
            text-align: center;
            border-radius: 1ch;
            /* height: 100px; */
        }

        .subscribe:hover {
            background-color: teal;
            color: white;
        }

        .subscribe h3 h4 {
            margin: 0;
            padding: 0;
        }
    </style>
@endpush

@push("scripts")
    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <!-- Chart code -->
    <script>
        am5.ready(function () {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv1");

            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);

            // Create chart
            // https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: true,
                panY: true,
                wheelX: "panX",
                wheelY: "zoomX",
                pinchZoomX: true,
                paddingLeft: 0,
                paddingRight: 1
            }));

            // Add cursor
            // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
            var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
            cursor.lineY.set("visible", false);


            // Create axes
            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xRenderer = am5xy.AxisRendererX.new(root, {
                minGridDistance: 30,
                minorGridEnabled: true
            });

            xRenderer.labels.template.setAll({
                rotation: -90,
                centerY: am5.p50,
                centerX: am5.p100,
                paddingRight: 15
            });

            xRenderer.grid.template.setAll({
                location: 1
            })

            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                maxDeviation: 0.3,
                categoryField: "country",
                renderer: xRenderer,
                tooltip: am5.Tooltip.new(root, {})
            }));

            var yRenderer = am5xy.AxisRendererY.new(root, {
                strokeOpacity: 0.1
            })

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                maxDeviation: 0.3,
                renderer: yRenderer
            }));

            // Create series
            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                name: "Series 1",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "value",
                sequencedInterpolation: true,
                categoryXField: "country",
                tooltip: am5.Tooltip.new(root, {
                    labelText: "{valueY}"
                })
            }));

            series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5, strokeOpacity: 0 });
            series.columns.template.adapters.add("fill", function (fill, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });

            series.columns.template.adapters.add("stroke", function (stroke, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });


            // Set data
            var data = [{
                country: "Pasta Gigi",
                value: parseInt(Math.random() * 1000)
            }, {
                country: "Beras Uduk 5Kg",
                value: parseInt(Math.random() * 1000)
            }, {
                country: "Minyak 5L",
                value: parseInt(Math.random() * 1000)
            }, {
                country: "Gas 3Kg",
                value: parseInt(Math.random() * 1000)
            }];

            xAxis.data.setAll(data);
            series.data.setAll(data);


            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            series.appear(1000);
            chart.appear(1000, 100);

        }); // end am5.ready()
    </script>

    <!-- Chart code -->
    <script>
        am5.ready(function () {

            // Create root element
            // https://www.amcharts.com/docs/v5/getting-started/#Root_element
            var root = am5.Root.new("chartdiv2");

            // Set themes
            // https://www.amcharts.com/docs/v5/concepts/themes/
            root.setThemes([
                am5themes_Animated.new(root)
            ]);

            // Create chart
            // https://www.amcharts.com/docs/v5/charts/xy-chart/
            var chart = root.container.children.push(am5xy.XYChart.new(root, {
                panX: true,
                panY: true,
                wheelX: "panX",
                wheelY: "zoomX",
                pinchZoomX: true,
                paddingLeft: 0,
                paddingRight: 1
            }));

            // Add cursor
            // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
            var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
            cursor.lineY.set("visible", false);


            // Create axes
            // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
            var xRenderer = am5xy.AxisRendererX.new(root, {
                minGridDistance: 30,
                minorGridEnabled: true
            });

            xRenderer.labels.template.setAll({
                rotation: -90,
                centerY: am5.p50,
                centerX: am5.p100,
                paddingRight: 15
            });

            xRenderer.grid.template.setAll({
                location: 1
            })

            var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
                maxDeviation: 0.3,
                categoryField: "country",
                renderer: xRenderer,
                tooltip: am5.Tooltip.new(root, {})
            }));

            var yRenderer = am5xy.AxisRendererY.new(root, {
                strokeOpacity: 0.1
            })

            var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
                maxDeviation: 0.3,
                renderer: yRenderer
            }));

            // Create series
            // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
            var series = chart.series.push(am5xy.ColumnSeries.new(root, {
                name: "Series 1",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "value",
                sequencedInterpolation: true,
                categoryXField: "country",
                tooltip: am5.Tooltip.new(root, {
                    labelText: "{valueY}"
                })
            }));

            series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5, strokeOpacity: 0 });
            series.columns.template.adapters.add("fill", function (fill, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });

            series.columns.template.adapters.add("stroke", function (stroke, target) {
                return chart.get("colors").getIndex(series.columns.indexOf(target));
            });


            // Set data
            var data = [{
                country: "Pasta Gigi",
                value: parseInt(Math.random() * 1000)
            }, {
                country: "Beras Uduk 5Kg",
                value: parseInt(Math.random() * 1000)
            }, {
                country: "Minyak 5L",
                value: parseInt(Math.random() * 1000)
            }, {
                country: "Gas 3Kg",
                value: parseInt(Math.random() * 1000)
            }];

            xAxis.data.setAll(data);
            series.data.setAll(data);


            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            series.appear(1000);
            chart.appear(1000, 100);

        }); // end am5.ready()
    </script>
@endpush